const sharedPresets = ['@babel/typescript'];

export default {
  presets: sharedPresets,
  env: {
    esm: {
      presets: sharedPresets,
    },
    cjs: {
      presets: [
        [
          '@babel/env',
          {
            modules: 'commonjs',
          },
        ],
        ...sharedPresets,
      ],
    },
  },
};
