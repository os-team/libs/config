# config

ESLint and Prettier shared configs.

## Installation

### Step 1. Install the ESLint config

Install one of the ESLint config by following the instructions:

- [eslint-config-os-team-node](https://gitlab.com/os-team/libs/config/-/tree/main/packages/eslint-config-os-team-node) – ESLint's rules for Node.js.
- [eslint-config-os-team-react](https://gitlab.com/os-team/libs/config/-/tree/main/packages/eslint-config-os-team-react) – ESLint's rules for React.
- [eslint-config-os-team-react-native](https://gitlab.com/os-team/libs/config/-/tree/main/packages/eslint-config-os-team-react-native) – ESLint's rules for React Native.

### Step 2. Install the Prettier config

Install the [@os-team/prettier-config](https://gitlab.com/os-team/libs/config/-/tree/main/packages/prettier-config) by following the instructions.

### Step 3. Add scripts to `package.json`

Append the following scripts to the existing scripts in your `package.json`:

```json
{
  "scripts": {
    "lint": "eslint --fix .",
    "lint-check": "eslint .",
    "pretty": "prettier --write --ignore-unknown .",
    "pretty-check": "prettier --check ."
  }
}
```

### Step 4. Install the `lint-staged`

[Lint-staged](https://github.com/okonet/lint-staged) allows to lint only those files that will be committed.

Install the `lint-staged` in devDependencies (-D) using the following command:

```
yarn add -D lint-staged
```

Create the `.lintstagedrc` file with the following content:

```json
{
  "**/*.{js,jsx,ts,tsx}": ["yarn lint", "yarn pretty"],
  "**/*.{json,md}": ["yarn pretty"]
}
```

### Step 5. Install the `husky`

[Husky](https://github.com/typicode/husky) allows you to automatically format your code with ESLint and Prettier before committing with Git hooks.

Install the `husky` in devDependencies (-D) using the following command:

```
yarn add -D husky
```

Enable Git hooks:

```
npx husky install
```

Add the `prepare` script to install Git hooks every time the `yarn install` command is called as follows:

```json
{
  "scripts": {
    "prepare": "husky"
  }
}
```

Create the `.husky/pre-commit` file with the following content:

```
yarn test
yarn lint-staged
```

Make this file executable:

```
chmod +x .husky/pre-commit
```
