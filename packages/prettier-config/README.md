# @os-team/prettier-config [![NPM version](https://img.shields.io/npm/v/@os-team/prettier-config)](https://yarnpkg.com/package/@os-team/prettier-config)

Prettier config.

## Installation

### Prerequisites

Install the exact version (-E) of `prettier` in devDependencies (-D) using the following command:

```
yarn add -DE prettier
```

### Step 1. Install the package

Install the exact version (-E) of the package in devDependencies (-D) using the following command:

```
yarn add -DE @os-team/prettier-config
```

### Step 2. Add the config

The way you add the prettier config to your project depends on whether you want to override some properties or not.

#### Option 1. Using the default config

Create the `.prettierrc` file with the following content

```json
"@os-team/prettier-config"
```

#### Option 2. Overriding some properties

Create the `.prettierrc.js` file with the following content

```js
module.exports = {
  ...require('@os-team/prettier-config'),
  semi: false,
};
```

In this example, you are overriding the `semi` property.
