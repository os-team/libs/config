# eslint-config-os-team-react-native [![NPM version](https://img.shields.io/npm/v/eslint-config-os-team-react-native)](https://yarnpkg.com/package/eslint-config-os-team-react-native)

ESLint rules for React Native, including TypeScript.

## Installation

### Step 1. Install the package

Install the exact version (-E) of the package in devDependencies (-D) using the following command:

```
yarn add -DE eslint-config-os-team-react-native
```

Make sure `eslint`, `jest`, and `react` packages are installed.

### Step 2. Extend your ESLint config

```js
const reactNativeConfig = require('eslint-config-os-team-react-native');

module.exports = [
  ...reactNativeConfig.configs.recommended,
  // Your config
  {
    files: ['**/*.{js,mjs,cjs,ts,jsx,tsx}'],
    rules: {},
  },
];
```
