import jsConfig from '@eslint/js';
import tsPlugin from 'typescript-eslint';
import importPlugin from 'eslint-plugin-import';
import prettierConfig from 'eslint-config-prettier';
import type { Linter } from 'eslint';

function recommended(configs: Linter.Config[] = []): Linter.Config[] {
  return [
    jsConfig.configs.recommended,
    ...tsPlugin.configs.recommended,
    importPlugin.flatConfigs.recommended,
    importPlugin.flatConfigs.typescript,
    {
      name: 'os-team/base',
      languageOptions: {
        ecmaVersion: 'latest',
        sourceType: 'module',
      },
      linterOptions: {
        reportUnusedDisableDirectives: 'error',
      },
      settings: {
        'import/resolver': {
          typescript: true,
          node: true,
        },
      },
    },
    {
      rules: {
        '@typescript-eslint/no-unused-vars': [
          'error',
          {
            args: 'all',
            argsIgnorePattern: '^_',
            caughtErrors: 'all',
            caughtErrorsIgnorePattern: '^_',
            destructuredArrayIgnorePattern: '^_',
            varsIgnorePattern: '^_',
            ignoreRestSiblings: true,
          },
        ],
        // TODO: Remove when the issue will be solved: https://github.com/eslint/eslint/issues/19134
        '@typescript-eslint/no-unused-expressions': [
          'error',
          {
            allowShortCircuit: true,
            allowTernary: true,
          },
        ],
      },
    },
    ...configs,
    prettierConfig,
  ].map((config) => ({
    files: ['**/*.{js,mjs,cjs,ts,jsx,tsx}'],
    ...config,
  }));
}

export default {
  configs: {
    recommended,
  },
};
