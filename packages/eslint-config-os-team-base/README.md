# eslint-config-os-team-base [![NPM version](https://img.shields.io/npm/v/eslint-config-os-team-base)](https://yarnpkg.com/package/eslint-config-os-team-base)

Base ESLint rules, including TypeScript.

Contains the following rules:

- ESLint
- TypeScript
- Import

Use one of the configurations:

- [eslint-config-os-team-node](https://gitlab.com/os-team/libs/config/-/tree/main/packages/eslint-config-os-team-node) – ESLint rules for Node.js.
- [eslint-config-os-team-react](https://gitlab.com/os-team/libs/config/-/tree/main/packages/eslint-config-os-team-react) – ESLint rules for React.
- [eslint-config-os-team-react-native](https://gitlab.com/os-team/libs/config/-/tree/main/packages/eslint-config-os-team-react-native) – ESLint rules for React Native.
