# eslint-config-os-team-node [![NPM version](https://img.shields.io/npm/v/eslint-config-os-team-node)](https://yarnpkg.com/package/eslint-config-os-team-node)

ESLint rules for Node.js, including TypeScript.

## Installation

### Step 1. Install the package

Install the exact version (-E) of the package in devDependencies (-D) using the following command:

```
yarn add -DE eslint-config-os-team-node
```

Make sure `eslint` and `jest` packages are installed.

### Step 2. Extend your ESLint config

```js
const nodeConfig = require('eslint-config-os-team-node');

module.exports = [
  ...nodeConfig.configs.recommended,
  // Your config
  {
    files: ['**/*.{js,mjs,cjs,ts,jsx,tsx}'],
    rules: {},
  },
];
```
