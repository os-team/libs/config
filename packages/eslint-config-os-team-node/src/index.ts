import baseConfig from 'eslint-config-os-team-base';
import globals from 'globals';

const recommended = baseConfig.configs.recommended([
  {
    name: 'os-team/node',
    languageOptions: {
      globals: {
        ...globals.node,
      },
    },
  },
]);

export default {
  configs: {
    recommended,
  },
};
