import baseConfig from 'eslint-config-os-team-base';
import reactPlugin from 'eslint-plugin-react';
import reactHooksPlugin from 'eslint-plugin-react-hooks';
import jsxA11yPlugin from 'eslint-plugin-jsx-a11y';
import globals from 'globals';

const recommended = baseConfig.configs.recommended([
  reactPlugin.configs.flat?.recommended || {},
  reactPlugin.configs.flat?.['jsx-runtime'] || {},
  {
    plugins: {
      'react-hooks': reactHooksPlugin,
    },
    rules: reactHooksPlugin.configs.recommended.rules,
  },
  jsxA11yPlugin.flatConfigs.recommended,
  {
    name: 'os-team/react',
    languageOptions: {
      globals: {
        ...globals.browser,
        ...globals.serviceworker,
      },
    },
    settings: {
      react: {
        version: 'detect',
      },
    },
    rules: {
      // To avoid an error when using React.FC<T>.
      'react/prop-types': 'off',
      // Sometimes it's better to set auto focus.
      'jsx-a11y/no-autofocus': 'off',
    },
  },
]);

export default {
  configs: {
    recommended,
  },
};
