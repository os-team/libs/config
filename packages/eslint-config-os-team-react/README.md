# eslint-config-os-team-react [![NPM version](https://img.shields.io/npm/v/eslint-config-os-team-react)](https://yarnpkg.com/package/eslint-config-os-team-react)

ESLint rules for React, including TypeScript.

## Installation

### Step 1. Install the package

Install the exact version (-E) of the package in devDependencies (-D) using the following command:

```
yarn add -DE eslint-config-os-team-react
```

Make sure `eslint`, `jest`, and `react` packages are installed.

### Step 2. Extend your ESLint config

```js
const reactConfig = require('eslint-config-os-team-react');

module.exports = [
  ...reactConfig.configs.recommended,
  // Your config
  {
    files: ['**/*.{js,mjs,cjs,ts,jsx,tsx}'],
    rules: {},
  },
];
```
